<?php

/**
 * Slides model config
 */

return array(

    'title' => 'Users',

    'single' => 'User',

    'model' => '\\App\\User',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'email',
        'name',
        'admin' => array(
            'type' => 'bool',
            'title' => 'Is Admin',
        ),
    ),

    /**
     * The filter set
     */
    'filters' => array(
        'id',
        'email',
        'name',
        'admin' => array(
            'type' => 'bool',
            'title' => 'Is Admin',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'email',
        'name',
        'password' => array(
            'type' => 'password',
            'title' => 'Password',
        ),
        'admin' => array(
            'type' => 'bool',
            'title' => 'Is Admin',
        ),
    ),
);