<?php

/**
 * Slides model config
 */

return array(

    'title' => 'Products',

    'single' => 'Product',

    'model' => '\\App\\Product',

    /**
     * The display columns
     */
    'columns' => array(
        'id' => array(
            'title' => 'Product ID',
            ),
        'name' => array(
            'title' => 'Product Name',
            ),
        'price' => array(
            'title' => 'Product Price',
            ),
        'main_image' => array(
            'title' => 'Image',
            ),
        'category' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => "(:table).category",
        ),
        'weight' => array(
            'title' => 'Weight'
        ),
    ),

    /**
     * The filter set
     */
    'filters' => array(
        'id' => 'Product ID',
        'name' => 'Product Name',
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => 'Product Name',
        'category' => array(
            'type' => 'relationship',
            'title' => 'Category',
            'name_field' => 'category', //what column or accessor on the other table you want to use to represent this object
        ),
        'weight' => array(
            'title' => 'Weight')
    ),
);