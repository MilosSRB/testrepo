<?php

/**
 * Categories model config
 */
return array(
    'title'   => 'Categories',
    'single'  => 'Category',
    'model'   => '\\App\\Category',
    /**
     * The display columns
     */
    'columns' => array(
        'id'         => array(
            'title' => 'Category ID',
        ),
        'category'   => array(
            'title' => 'Category Name',
        ),
    ),
    /**
     * The filter set
     */
    'filters' => array(
        'id'       => 'Category ID',
        'category' => 'Category Name',
    ),
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'category'    => 'Category',
        'description' => array(
            'title' => 'Description',
            'type'  => 'wysiwyg',
        ),
        'sort'        => 'Sort',
    ),
);