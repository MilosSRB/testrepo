<?php
namespace App;

class Cart {
	const SESSION_KEY = 'cart';

	/*
		array(
			product_id => qty
			'1' => 2
		)
	*/

	public static function getAllItems() {
		$items = \Session::get(self::SESSION_KEY);

		return $items;
	}

	public static function add($product_id, $qty) {
		$items = self::getAllItems();

		$items[] = array(
			'priduct_id' => $product_id,
			'qty' 		 => $qty,
		);
		\Session::put(self::SESSION_KEY, $items);
	}

	public static function delete($product_id) {

	}

	public static function deleteAll() {

	}

	public static function update($product_id, $qty) {

	}
}