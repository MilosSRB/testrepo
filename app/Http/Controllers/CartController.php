<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Cart;
use App\Prduct;

class CartController extends Controller {

	public function view() {
		$items = Cart::getAllItems();

		foreach ($items as $key => $item) {
			$items[$key]['product'] = Product::find($item['product_id']);
		}

		return view('cart_view', compact('items'));
	}

	public function addToCart($product_id) {
		$product_id = \Request::input('product_id');
		$qty = \Request::input('qty');

		if (!$product_id or !$qty) {
			return redirect('/');
		}

		Cart::add($product_id, $qty);

		return redirect(route('cart_view'));
	}
}