<?php namespace App\Http\Controllers;

use \App\Category;
use \App\Slide;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$slides = Slide::where('enable', '=', '1')->get();


		//SELECT * FROM categories where level = 0
		$categories = Category::where('level', '=', '0');

		return view('welcome', compact('slides', 'categories'));
	}
}
