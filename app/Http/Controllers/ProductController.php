<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Product;

class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($product_id)
	{
		$product = Product::find($product_id);

        if (!$product) {
            // return view('not-found');
            return redirect('/');
        }

        return view('product', compact('product'));
	}

}