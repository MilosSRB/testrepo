<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use \App\Category;

class Categories extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function view($category_id)
	{
		$category = Category::find($category_id);
		if (!$category) {
			return redirect('/');
		}

		return view('category', compact('category'));
	}
}