<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model {

	//

	public function getImagePublicSrcAttribute()
	{
		return url(self::getImagePath() . $this->image);
	}

	public static function getImagePath()
	{
		return '/images/slides/';
	}
}
