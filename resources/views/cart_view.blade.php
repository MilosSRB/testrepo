@extends('app')

@section('content')
<div class="container" style="width: 80%">
	<h2>My Cart</h2>

	@if (count($items) == 0)
		There are no items in your cart.
	@else
		@foreach ($items as $item)
			<div>
				<span>{{ $item['product']->name }}</span>
				<span>{{ $item['qty'] }}</span>
			</div>
		@endforeach
	@endif
<div>
@endsection