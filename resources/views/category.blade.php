@extends('app')

@section('content')
<div class="container">
	<h2>{{$category->category }}</h2>

	@foreach ($category->products()->get() as $product)
		<div>
			{{ $product->name }}
		</div>
	@endforeach
</div>
@endsection