@extends ('app')
@section ('content')
<!-- Homepage Slider -->
        <div class="homepage-slider">
        	<div id="sequence">
				<ul class="sequence-canvas">
					<!-- Slide 1 -->
					<li class="bg4">
						<!-- Slide Title -->
						<h2 class="title">Rocket 1</h2>
						<!-- Slide Text -->
						<h3 class="subtitle">Best built Rocket that you will find on the market!</h3>
						<!-- Slide Image -->
						<img class="slide-img" src="/public/img/homepage-slider/Slide1.png" alt="Slide 1" />
					</li>
					<!-- End Slide 1 -->
					<!-- Slide 2 -->
					<li class="bg3">
						<!-- Slide Title -->
						<h2 class="title">Rocket2</h2>
						<!-- Slide Text -->
						<h3 class="subtitle">Comes with 5 color schemes and it's easy to make your own!</h3>
						<!-- Slide Image -->
						<img class="slide-img" src="/public/img/homepage-slider/slide2.png" alt="Slide 2" />
					</li>
					<!-- End Slide 2 -->
					<!-- Slide 3 -->
					<li class="bg1">
						<!-- Slide Title -->
						<h2 class="title">Rocket3</h2>
						<!-- Slide Text -->
						<h3 class="subtitle">Huge amount of components and over 30 sample for free!</h3>
						<!-- Slide Image -->
						<img class="slide-img" src="/public/img/homepage-slider/slide3.png" alt="Slide 3" />
					</li>
					<!-- End Slide 3 -->
				</ul>
				<div class="sequence-pagination-wrapper">
					<ul class="sequence-pagination">
						<li>1</li>
						<li>2</li>
						<li>3</li>
					</ul>
				</div>
			</div>
        </div>
        <!-- End Homepage Slider -->

		<!-- Press Coverage -->
        <div class="section">
	    	<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="in-press press-wired">
							<a href="#">In modern military usage, a missile, or guided missile, is a self-propelled precision-guided munition system, as opposed to an unguided self-propelled munition, referred to as a rocket. Missiles have four system components: targeting and/or missile guidance, flight system, engine, and warhead.  </a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="in-press press-mashable">
							<a href="#">Missiles come in types adapted for different purposes: surface-to-surface and air-to-surface missiles (ballistic, cruise, anti-ship, anti-tank, etc.), surface-to-air missiles (and anti-ballistic), air-to-air missiles, and anti-satellite weapons.</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="in-press press-techcrunch">
							<a href="#">All known existing missiles are designed to be propelled during powered flight by chemical reactions inside a rocket engine, jet engine, or other type of engine.</a>
						</div>
					</div>
				</div>
			</div>
		</div>
       <!-- Our Clients -->
	    <div class="section">
	    	<div class="container">
	    		<h2>Follow Us</h2>
				<div class="clients-logo-wrapper text-center row">
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/FaceBook.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/Twitter.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/LinkedIn.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/GooglePlus.png" alt="Client Name"></a></div>
					
				</div>
			</div>
	    </div>
	    <!-- End Our Clients -->
	    @endsection