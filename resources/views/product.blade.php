@extends('app')

@section('content')
<div class="container" style="width: 80%">
    <div class="row">
        <img src="{{ url('images/sample-product.png') }}" class="img-responsive img-thumbnail" align="left" />
        <div style="margin-left: 325px">
            <h2>{{ $product->name }}</h2>
            <h3>${{ $product->price }}</h3>
            <form method="POST" action="{{ route('cart_add') }}">
                <hr />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                Qty: <input type="text" name="qty" value="1" />
                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                <input
                    type="submit"
                    class="btn btn-success pull-right"
                    style="margin-right: 10%"
                    value="Add to Cart!"
                /> 
                <hr />
            </form>
        </div>
    </div>
</div>
@endsection