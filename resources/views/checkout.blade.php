@extends('app')

@section('content')

<div class="container" style="width: 80%">
		<h2>Checkout</h2>

		<form method="POST" action="#" id="checkout">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<fieldset>
				<legend>Billing Address</legend>
				<dl>
					<dt>First Name</dt>
					<dd><input name="billing_first_name" type="text" /></dd>

					<dt>Last Name</dt>
					<dd><input name="billing_last_name" type="text" /></dd>
					
					<dt>Address</dt>
					<dd><input name="billing_address" type="text" /></dd>

					<dt>City</dt>
					<dd><input name="billing_city" type="text" /></dd>

					<dt>Zip</dt>
					<dd><input name="billing_zip" type="text" /></dd>

					<dt>State</dt>
					<dd><input name="billing_state" type="text" /></dd>
				</dl>
			</fieldset>

			<fieldset>
				<legend>Shipping Address</legend>
				<dl>
					<dt>First Name</dt>
					<dd><input name="shipping_first_name" type="text" /></dd>

					<dt>Last Name</dt>
					<dd><input name="shipping_last_name" type="text" /></dd>
					
					<dt>Address</dt>
					<dd><input name="shipping_address" type="text" /></dd>

					<dt>City</dt>
					<dd><input name="shipping_city" type="text" /></dd>

					<dt>Zip</dt>
					<dd><input name="shipping_zip" type="text" /></dd>

					<dt>State</dt>
					<dd><input name="shipping_state" type="text" /></dd>
				</dl>
			</fieldset>

			<fieldset>
				<legend>Shipping Method</legend>

				<dt>UPS Ground: $10.00</dt>
			</fieldset>

			<fieldset>
				<legend>Payment Method</legend>
				 <div class="form-row">
                <label>Card Number</label>
                <input type="text" maxlength="20" autocomplete="off" class="card-number stripe-sensitive required" />
            </div>
            
            <div class="form-row">
                <label>CVC</label>
                <input type="text" maxlength="4" autocomplete="off" class="card-cvc stripe-sensitive required" />
            </div>
            
            <div class="form-row">
                <label>Expiration</label>
                <div class="expiry-wrapper">
                    <select class="card-expiry-month stripe-sensitive required">
                    </select>
                    
                </div>
            </div>
 
            <button type="submit" name="submit-button">Submit</button>
            <span class="payment-errors"></span>
			</fieldset>
		</form>
	</div>
	@endsection